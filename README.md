
<!-- README.md is generated from README.Rmd. Please edit that file -->

# bbmixpaper\_pipeline

We developed the method BBmix (Bayesian beta-binomial mixture model) for
calling genotypes from RNA-seq. The manuscript describing the method can
be found [here](https://doi.org/10.1093/bioinformatics/btad393).

In this repo we provide the pipeline with the scripts used for the
analysis presented in the BBmix paper. In order to run the scripts you
will need to intall to R packages:

[bbmix](https://gitlab.com/evigorito/bbmix) is an R package with the
BBmix model.

[bbmix\_paper](https://gitlab.com/evigorito/bbmix_paper) in an R package
with the required functions for running the analysis presented in the
paper.

## System requirements

    R versions >= 3.4.0.
    GNU make >= 3.82

## Instal bbmixpaper\_pipeline from [GitLab](https://gitlab.com):

``` r
## Within R:
install.packages("devtools") # if you don't already have the package
library(devtools)
devtools::install_git(url = "https://gitlab.com/evigorito/bbmixpaper_pipeline.git") 
```

## Instal bbmixpaper package from [GitLab](https://gitlab.com):

``` r
## Within R:
install.packages("devtools") # if you don't already have the package
library(devtools)
devtools::install_git(url = "https://gitlab.com/evigorito/bbmixpaper.git") 
```

## Instal bbmix package from [GitLab](https://gitlab.com):

``` r
## Within R:
install.packages("devtools") # if you don't already have the package
library(devtools)
devtools::install_git(url = "https://gitlab.com/evigorito/bbmix.git") 
```

## Workflow

We provide a
[snakemake](https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html)
with the code used to prepare the figures and tables in the paper in a
[Snakefile](https://gitlab.com/evigorito/bbmixpaper_pipeline/-/blob/master/Snakefile).

The scripts called from the
[Snakefile](https://gitlab.com/evigorito/bbmixpaper_pipeline/-/blob/master/Snakefile)
rules are in the subdirectory
[Scripts](https://gitlab.com/evigorito/bbmixpaper_pipeline/-/blob/master/Scripts)
