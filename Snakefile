#################################
###########################
## Code for BBmix paper
##########################
#################################


shell.prefix("source ~/.bashrc; ")

localrules: all

import pandas as pd
import os
import random
random.seed(10)

from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
FTP = FTPRemoteProvider()
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
HTTP = HTTPRemoteProvider()


# variables
chroms=[x+1 for x in range(22)]
samples_gb  # list with samples names in the  E-MTAB-1883 study
samples_geu # list with sample names for the GEUVADIS sub-sample used in this study
samples_peac # list with sample names for PEAC study 


#################################
# Download RNA-seq data for GEUVADIS-GBR
#################################

rule down_geu_metadata:
    """Download metadata for GEUVADIS RNA-seq data and select GBR samples (~100)""" 
    input:
        FTP.remote("ftp.ebi.ac.uk/pub/databases/arrayexpress/data/experiment/GEUV/E-GEUV-1/E-GEUV-1.sdrf.txt", keep_local=True)
    output:
        config['geu_dir'] + '/sample_info/GBR.samples'        
    shell:
        "mv {input} {output}2 ;"
        # get column number for column with sample names
        "col=$(cat {output}2 | awk -v RS='\t' '/Comment\[FASTQ_URI\]/{{print NR; exit}}') ;"
        # Select column with sample names for British sub-cohort
        "grep British {output}2 | cut -f 1,$col  > {output}; "
        "rm {output}2"
        

rule GBRinRP:
    """Check which of the GBR samples with RNA-seq data are in reference panel to get genotypes, 86 in total. Get a file with first col samples and second col fasq file name"""
    input:
        gbr=config['geu_dir'] + '/sample_info/GBR.samples',
        gt_sample=config['1000G'] + "/1000GP_Phase3.sample"
    output:
        config['geu_dir'] + '/sample_info/gbr_gt.txt'
    shell:
        "awk 'NR==FNR{{a[$1]; next}} $1 in a {{print $0}}' {input.gt_sample} {input.gbr} " 
        " > {output} "
        

rule down_geu:
    """Download RNA-seq files for the 86 GBR in reference panel"""
    input:
        config['geu_dir'] + '/sample_info/gbr_gt.txt'
    output:
        expand(config['geu_dir'] + '/RNA_seq_fastq/{fastq}', fastq=[os.path.basename(x) for f in d_geu.values() for x in f])
    shell:
        "cd {config[geu_dir]} ; "
        "cat {input} | cut -f2 | parallel -gnu \"wget {{}} \" "

# ################################################################### #
## Apply bbmix_pipeline to samples E-MTAB-1883, GEUVADIS-GBR and PEAC
# 
# Call genotypes using bbmix with model trained with default sample.
# Call genotypes with the same sample use for training,
# Call genotypes  with a pool of study samples.
# The file referred below as
# fsnps=config['output_dir'] + "/fSNPs/chr.{chrom}.fSNP.RP.genes.txt"
# is also prepared in the bbmix_pipeline.
# See  https://gitlab.com/evigorito/bbmix_pipeline and
# https://gitlab.com/evigorito/bbmix (R package)
# ################################################################## #

        
# ################################# #
# Call genotypes with haplotypeCaller  #
# ################################# #        
        
        
rule HapCaller:
    """Based on https://qcb.ucla.edu/wp-content/uploads/sites/14/2016/03/GATK_Discovery_Tutorial-Worksheet-AUS2016.pdf. Input bam is the output file from GATK pipeline. fasta and fsnps are detailed in bbmix_pipeline"""
    input:
        bam=config['output_dir'] + "/BRQC/{samp}.rmdup.sort.bam",
        fasta=config['ref_fasta'],
        fsnps=config['output_dir'] + "/fSNPS/fSNPs_{chrom}_RP_maf0_01.bed"
    output:
        vcf=config['output_dir'] + "/HapCaller/{samp}.{chrom}.g.vcf.gz",    
    shell:
        "{config[gatk]} --java-options \"-Xmx4g\" HaplotypeCaller "
        "-R {input.fasta} "
        "-I {input.bam} "
        "-L {input.fsnps} "
        "--dont-use-soft-clipped-bases true "       
        "-ip 100 "
        "-O {output.vcf} "
        "-ERC GVCF "


rule sample_map:
    """Prepares a tab separated map file with sample_name--tab--path_to_sample_vcf per line to use for GenomicsDBImport rule. Asumes sample names have no "." """
    input:
        gvcf=expand(config['output_dir'] + "/HapCaller/{samp}.{{chrom}}.g.vcf.gz", samp=samples_gb),  
    output:
        samp_map=config['output_dir'] + "/samp_map/{chrom}.tab"
    run:
        keys=[os.path.basename(x).split(".")[0] for x in input.gvcf]
        df = pd.DataFrame(list(zip(keys, input.gvcf)),
                          columns =['samp', 'path'])
        df.sort_values(by=['samp'], inplace=True)
        df.to_csv(output.samp_map, sep='\t', index=False, header=False)
        

rule GenomicsDBImport:
    """Consolidate gvcf files from different samples by chromosomes to call variants. There is an interference between snakemake and this tool when creating the directory to store data. If snakmake creates it I get an error from the tool. To overcome that I use as output file a temporary file that helps to keep temporal order with next rule and get deleted as it is no needed."""
    input:
        sample_map=config['output_dir'] + "/samp_map/{chrom}.tab"
    params:
        out_dir=config['output_dir'] + "/GDBImport/chr{chrom}",
        out_int=config['output_dir'] + "/GDBImport"
    output:
        out=temp(config['output_dir'] + "/tmp/chr{chrom}")
    shell:
        "mkdir -p {params.out_int} ; "
        "{config[gatk]} --java-options \"-Xmx4g -Xms4g\" "
        "GenomicsDBImport "
        "--genomicsdb-workspace-path {params.out_dir} "
        "--overwrite-existing-genomicsdb-workspace true "
        "-L {wildcards.chrom} "
        "--sample-name-map {input.sample_map} ;"
        "touch {output.out} "

rule gGVCF:
    input:
        fasta=config['ref_fasta'],
        tmp=config['output_dir'] + "/tmp/chr{chrom}",
    params:
        gdbi=config['output_dir'] + "/GDBImport/chr{chrom}"
    output:
        vcf=config['output_dir'] + "/GT_joint/chr{chrom}.vcf.gz",    
    shell:
        "{config[gatk]} --java-options \"-Xmx4g\" GenotypeGVCFs "
        "-R {input.fasta} "
        "-V gendb://{params.gdbi} "
        "-stand-call-conf 20.0 "
        "-O {output.vcf} "


rule variant_filtration:
    """Use hard call thresholds recommended by gatk to filter variants called by RNA-seq. http://barcwiki.wi.mit.edu/wiki/SOP/CallingVariantsRNAseq  https://igor.sbgenomics.com/public/apps?__hstc=64521200.7fdfc2b50ce2ea62a2d2adaf0a9a4f39.1612344585113.1612344585113.1619434967272.2&__hssc=64521200.1.1619434967272&__hsfp=1616484006#admin/sbg-public-data/broad-best-practices-rna-seq-variant-calling-4-1-0-0/ """
    input:
        fasta=config['ref_fasta'],
        vcf=config['output_dir'] + "/GT_joint/chr{chrom}.vcf.gz",
    output:
        vcf=config['output_dir'] + "/GT_joint_filtered/chr{chrom}.vcf.gz",
    shell:
        "{config[gatk]} VariantFiltration "
        "-R {input.fasta} "
        "-V {input.vcf} "
        "-O {output.vcf} "
        "-window 35 -cluster 3 "
        "--filter-expression \"FS > 30.0 || QD < 2.0\" "
        "--filter-name \"FS,QD\" "


rule VarsQC_joint_filt:
    """Extract info from vcf after calling genotypes jointly"""
    input:
        vcf=config['output_dir'] + "/GT_joint_filtered/chr{chrom}.vcf.gz", 
    output:
        tab=config['output_dir'] + "/GTQC_joint_filt/chr{chrom}.table",
    shell:
        "{config[gatk]} VariantsToTable "
        "-V {input.vcf} "
        "-F CHROM -F POS -F REF -F ALT  -F MQ -F SOR -GF GT  -GF PL  -GF DP -GF AD "

# #################################### #
# Call variants using bcftools mpileup #
# #################################### #

rule list_bam:
    """ Prepare list of bam files to rename callVar vcf below. Format is full path to bam and individual ID in each line space delimited. Also prepare bamPath, file with full name to bam files, input for calling variants"""
    input:
        bam=expand(config['output_dir'] + "/BQRC/{samp}.rmdup.sort.bam", samp=samples_gb),
    output:
        bamSampNames=config['output_dir'] + "/BRQC/bamSampleNames.txt",
        bamPath=config['output_dir'] + "/BQRC/Path2bam.txt"
    script:
        "Scripts/bamRename.py"       


rule RNA_callVar:
    """Call variants from RNA only for fSNPs (bed file). 4 shell commands to call genotypes, rename samples, compress and index """
    input:
        bam=expand(config['output_dir'] + "/BQRC/{samp}.rmdup.sort.bam", samp=samples_gb),
        fasta=config['ref_fasta'],
        bed=config['output_dir'] + "/fSNPS/fSNPs_{chr}_RP_maf0_01.bed",
        bamList=config['output_dir'] + "/BRQC/bamSampleNames.txt",
        bamPath=config['output_dir'] + "/BRQC/Path2bam.txt"
    output:
        vcf=config['output_dir'] + "/GT_mpileup/chr{chr}_Q20.vcf.gz",
    params:
        QUAL=20
    shell:
        "tmp=$(echo {config[output_dir]}/genome_bottle/GT_mpileup/chr{wildcards.chr}_Q20.vcf) ; "
        "tmp2=$(echo {config[output_dir]}/genome_bottle/GT_mpileup/chr{wildcards.chr}_Q20_v2.vcf) ; "
        "bcftools mpileup "
        "-R {input.bed} -Ou "
        "-f {input.fasta} "
        "-b {input.bamPath} "
        "-a DP -aDP -I "
        "--threads {threads} | "
        "bcftools call -mv -Ou --threads {threads} | "
        "bcftools view -i '%QUAL>={params.QUAL}' --threads {threads} -Ov -o $tmp ; "
        "bcftools reheader -s {input.bamList} $tmp > $tmp2; "
        "bgzip -c $tmp2 > {output.vcf} ; "
        "tabix -p vcf {output.vcf} ; "
        "rm $tmp ;"
        "rm $tmp2;"
        
rule table_variants:
    """ Extract GT and DP from vcf for downstream analysis """
    input:
        vcf=config['output_dir'] + "/GT_mpileup/chr{chr}_Q20.vcf.gz",    
    output:
        table=config['output_dir'] + "/GT_mpileup/chr{chr}_Q20.table",   
    shell:
        "{config[gatk]} VariantsToTable "
        "-V {input.vcf} "
        "-F CHROM -F POS -F REF -F ALT -GF GT  -GF DP "
        "-O {output.table} "

# #################################### #
# Call variants using freebayes #
# #################################### #              

# FreeBayes removes duplicates but doesnt use the whole GATK pipeline (https://github.com/freebayes/freebayes)
# Need to add ID to files after removing dups and then run them for freebayes

rule addGroup_free_and_index:
    """FreeBayes requires read group with sample name to identify alignments"""
    input:
        config['output_dir'] + "/rmdup/{samp}.rmdup.sort.bam"
    output:
        config['output_dir'] + "/freebayes/{samp}.rmdup.sort.bam"
    shell:
        "{config[gatk]} AddOrReplaceReadGroups "
        "-I {input} "
        "-O {output} "
        "-RGID {wildcards.samp} "
        "-RGLB {wildcards.samp} "
        "-RGPL illumina "
        "-RGPU {wildcards.samp} "
        "-RGSM {wildcards.samp}; "
        "samtools index {output[0]}"

rule freebayes:
    """Run freebayes. I set up freebayes in a separate environment as snakemake to avoid any conflict. I need to activate my freebayes environment before calling freebayes. You may omit this step"""
    input:
        bam=expand(config['output_dir'] + "/freebayes/{samp}.rmdup.sort.bam", samp=samples_gb),
        fsnps=config['output_dir'] + "/fSNPS/fSNPs_{chrom}_RP_maf0_01.bed",
        fasta=config['ref_fasta'],
    output:
        config['output_dir'] + "/freebayes/chr.{chrom}.vcf.gz"
    shell:
        "CONDA_BASE=$(conda info --base); " # get path to conda
        "source $CONDA_BASE/etc/profile.d/conda.sh; " # get functions
        "conda activate freebayes ; "                 # activate environment
        "freebayes -f {input.fasta} -t {input.fsnps} {input.bam} | "
        "vcffilter -f \"QUAL > 20\" -f \"DP > 9 \" | "
        "bcftools view -Oz >  {output} ; "
        "conda deactivate ; "

rule free_table:
    """Extract information from freebayes calls into table format"""
        """ Extract GT and DP from vcf for downstream analysis """
    input:
        vcf=config['output_dir'] + "/freebayes/chr.{chrom}.vcf.gz",    
    output:
        table=config['output_dir'] + "/freebayes/chr.{chrom}.table",   
    shell:
        "tabix -p vcf {input.vcf} ; "
        "{config[gatk]} VariantsToTable "
        "-V {input.vcf} "
        "-F CHROM -F POS -F REF -F ALT -GF GT -GF DP  "
        "-O {output.table} "

rule rem_hom_na:
    """For FreeBayes calls remove SNPs if ref-homozygous calls in all samples or re-hom/na to make a fair comparison with GATK and mpileup"""
    input:
        table=config['output_dir'] + "/freebayes/chr.{chrom}.table",
    output:
        out=config['output_dir'] + "/freebayes/ex_hom_ref/chr.{chrom}.table",
    script:
        "Scripts/rem_ref_hom_na.R"
   


# ############### #
# Gold standards  #
# ############### #   


# #################################### #
# Select fSNP genotypes in genome bottle gold standard#
# #################################### #        

rule download_bottle:
    """Download genome in a bottle high confidence calls """
    input:
        FTP.remote("ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release//NA12878_HG001/latest/README_NISTv3.3.2.txt", keep_local=True),FTP.remote(expand("ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release//NA12878_HG001/latest/GRCh37/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_{file}", file=["nosomaticdel.bed","PGandRTGphasetransfer.vcf.gz", "PGandRTGphasetransfer.vcf.gz.tbi"] ), keep_local=True)
    output:
        config['bottle_dir'] + "/GT/README_NISTv3.3.2.txt"
    run:
        shell("mv {input} {config[bottle_dir]}")
        
rule bed_fSNPs:
    """Subset fSNPs from gold standard and make it tab format for easy access. First need to append bed files with fSNPs positions. The input file was prepapred in bbmix_pipeline"""
    input:
        fsnps=expand(config['output_dir'] + "/fSNPS/fSNPs_{chrom}_RP_maf0_01.bed", chrom=chroms),
    output:
        config['output_dir'] + "/fSNPS_combined/fSNPs_RP_maf0_01.bed"
    shell:
        "cat {input.fsnps} >> {output} "

rule subset_gold_standard_genome_bottle:
    """Subset fSNPs from gold standard and make it tab format for easy access."""
    input:
        vcf=config['bottle_dir'] + "/GT/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.vcf.gz",
        bed=config['output_dir'] + "/fSNPS_combined/fSNPs_RP_maf0_01.bed",
    output:
        config['bottle_dir'] + "/gold_standard_fSNPs/gs_gt_fsnps.tab"
    shell:
        "bcftools query -R {input.bed} "
        "-f '%CHROM\t%POS\t%REF\t%ALT[\t%GT]\n' "
        "{input.vcf} > {output} "


# ################################# #
# Select SNPs from GEUVADIS gold standard  #
# ################################# #        

rule ref_panel_alt:
    """ When formatting the 1000G legend/hap/sample into bcf using "bcftools convert -H ..." the column for the ALT allele was missing. In this rule using the legend file I added the missing ALT allele.
    step 1 counts the number of lines in bcf header (head variable).
    step 2 from legend file create array with keys line number and values ALT; excluding header (NR>1) from legend file. Then I add the ALT to the bcf but I need to exclude the header of the bcf file. I get the number of lines in "head". Then I start filling at FNR>head and the first ALT is in FNR-head+1. 
    step 3 indexes the bcf file. I am not matching by position and reference as I had entries with same position and reference but different ALT with results in errors. The legend file and the bcf are in the same order,  the bcf is transformed from the legend/hap/sample."""
    input:
         lambda wildcards: vcf(config["ref_bcf"])[wildcards.chrom],
         lambda wildcards: vcf(config["ref_legend"])[wildcards.chrom]
    output:
        config['output_dir'] + "/DNA/RP_chr{chrom}_alt_added.bcf" ,
        config['output_dir'] + "/DNA/RP_chr{chrom}_alt_added.bcf.csi"
    shell:
        "head=$(bcftools view -h {input[0]} | wc -l) ; "
        "hm1=$(($head-1)) ; "
        "awk -v head=$head -v hm1=$hm1 "
        " 'FNR==NR{{ if(NR>1) a[NR]=$4;next}}{{if(FNR > head) "
        " $5=a[((FNR - hm1)) ]}}1'  OFS='\\t' "
        " <(gzip -dc {input[1]}) <(bcftools view  {input[0]}) "
        " | bcftools view -Ob -o {output[0]}; "
        "tabix {output[0]} "

rule geu_fSNPs:
    """Use bcf files prepared  in which I added the alt allele to extract GT for GEUVADIS subsample and fSNPS. These files will be used as gold standard to compare with RNA-seq genotyping methods."""
    input:
        bcf=config['geu_bcf'] + "/DNA/RP_chr{chrom}_alt_added.bcf",
        fsnps=config['output_dir'] + "/fSNPS/fSNPs_{chrom}_RP_maf0_01.bed",
    params:
        samples=",".join(sorted(samples_geu))
    output:
        table=config['geu_dir'] + "/fsnp_GT/GBR_chr{chrom}.tab"  
    shell:
        "tmp=$(echo {config[geu_dir]}/quant/fsnp_GT/GBR_chr{wildcards.chrom}.vcf.gz) ;"
        "bcftools view -Oz -s {params.samples} {input.bcf} -o $tmp ; "
        "tabix $tmp ; "
        "{config[gatk]} VariantsToTable "
        "-V $tmp "
        "-L {input.fsnps} "
        "-F CHROM -F POS -F REF -F ALT -GF GT -GF DP "
        "-O {output.table} ; "
        "rm $tmp ;"
        "rm $tmp.tbi "


# ################################# #
# PEAC gold standard  #
# ################################# #            

rule fSNPs_dna:
    """From DNA vcf filter fSNPs"""
    input:
        dna=lambda wildcards: vcf_list[wildcards.chroms],
        fsnp=lambda wildcards: config['fSNP_dir'] + "/chr." + wildcards.chrom[3:]  + ".fSNP.RP.genes.txt"
    params:
        chromosome=lambda wildcards: wildcards.chrom[3:],
        query='"%CHROM %POS %ID %REF %ALT[ %GT] [ %GP]\\n"',
    output:
        dna=config['peac_dir'] + "/DNA/fSNP/{chrom}.fSNP.PEAC.txt"
    shell:
        "tmp=$(echo {config[peac_dir]}/DNA/fSNP/{wildcards.chrom}.fSNP); "
        "awk '{{print {params.chromosome}, $1, $1}}' OFS='\\t' {input.fsnp} > $tmp ; "
        "bcftools query -f {params.query} -R $tmp  {input.dna} > {output.dna} ;"
        "bcftools query -f {params.query} -H  {input.dna} | head -1  | cat - {output.dna} > $tmp ; "
        "mv $tmp {output.dna} "
        
        

rule fSNPs_rna_dna:
    """Select fSNPs in RNA and DNA called variants. RNA: recodes genotypes to 0-2, NA. DNA: makes hard calls if GP=1."""
    input:
        dna=config['peac_dir'] + "/DNA/fSNP/{chrom}.fSNP.PEAC.txt",
        rna=config['peac_dir'] + "/call_var/RPvar/{chrom}_Q20_filtRP.txt",
        rna_h=config['peac_dir'] + "/call_var/RPvar/{chrom}_Q20_filtRP.header.txt",
        fsnp=lambda wildcards: config['fSNP_dir'] + "/chr." + wildcards.chrom[3:]  + ".fSNP.RP.genes.txt",
    params:
        chrom=lambda wildcards: wildcards.chroms
    output:
        rna=config['peac_dir'] + "/call_var/fSNP/{chrom}.fSNPs.RNA.txt",
        dna=config['peac_dir'] + "/call_var/fSNP/{chrom}.fSNPs.DNA.txt"
    script:
        "Scripts/fsnp.select.R"



# ################################# #
# Paper figures  #
# ################################# #


rule prior_plot:
    """Make plot to visualise prior. For raw counts need to use a GEUVADIS file so I can split counts for each genotype, genome bottle doesnt have G=0."""
    input:
        counts=expand(config['geu_dir'] + "/quant/beta_binomial/allelic_counts_unique/{samp}.Q20.alleleCounts.txt", samp=samples_geu[0]),
        gs=expand(config['geu_dir'] + "/quant/fsnp_GT/GBR_chr{chrom}.tab", chrom=chroms),
        blackHapCounts=config['phaser_blackHapCounts']
    params:
        n=10000,
        a=[1,10,499],
        b=[499,10,1]
    output:
        out=config['output_dir'] + "/figures_paper/model_priors.png"
    script:
        "Scripts/model.prior.R"
        
rule input_posterior_mix_prior_paper:
    """Look at posterior distribution training beta binomial model with different number of fSNPs from genome in a bottle sample. Then check the effect in calling genotypes. I excluded i=1 because for 1 model (SNPs=500) the model didnt converge, so this way I have 3 repetitions for each N."""
    input:
        fit=expand(config['gb_dir'] + "/fit_beta_binom_/NA12878.{N}inds.{i}rep_stan_beta_binom_fit.rds",  N=[500, 1000,2000, 3000, 4000,  5000, 10000], i=[2,3,4]),
        gt=expand(config['gb_dir'] + "/beta_binom_gt_all_fit_yes/NA12878.{N}inds.{i}rep_chrom{chrom}.gt.txt",  N=[500, 1000,2000, 3000, 4000,  5000, 10000], i=[2,3,4], chrom =chroms),
        gs=config['bottle_dir'] + "/gold_standard_fSNPs/gs_gt_fsnps.tab",
        blackHapCounts=config['phaser_blackHapCounts'],
    output:
        out=config['output_dir'] + "/figures_paper/gb_training_N_SNPs_concordance_by_gt.png"
    script:
        "Scripts/gb_SNP_training_bbmix.R"        

rule methods_gb:
    """Compare performance of haplotypecaller, mpileup, count threshold
and beta binomial by depth using genome in a bottle sample. This will make Figures 2A-3A, Supp Figure 2,3."""
    input:
        hc=expand(config['bottle_dir'] + "/GTQC_joint_filt/chr{chrom}.table", chrom=chroms),
        mpileup=expand(config['bottle_dir'] + "/GT_mpileup/chr{chr}_Q20.table", chr=chroms),
        fb=expand(config['bottle_dir'] + "/freebayes/ex_hom_ref/chr.{chrom}.table",chrom=chroms),
        bb=expand(config['bottle_dir'] + "/beta_binom_gt_all_fit_yes/chrom{chrom}.gt.txt", chrom=chroms),
        gs=config['bottle_dir'] + "/gold_standard_fSNPs/gs_gt_fsnps.tab",
        blackHapCounts=config['phaser_blackHapCounts'],
    params:
        samp="NA12878"
    output:
        out=config['output_dir'] + "/figures_paper/gb_methods_compare.png"
    script:
        "Scripts/compare_methods_gb.R"


rule methods_geu:
    """Compare performance of haplotypecaller, mpileup, count threshold
and beta binomial by depth using geuvadis samples. This will make Figures 2B-3B, Supp Figure 4."""
    input:
        hc=expand(config['geu_dir'] + "/gatk/GTQC_joint_filt/chr{chrom}.table", chrom=chroms),
        mpileup=expand(config['geu_dir'] + "/mpileup/chr{chrom}_Q20.table", chrom=chroms),
        fb=expand(config['geu_dir'] + "/freebayes/chr.{chrom}.table",chrom=chroms),
        bb=expand(config['geu_dir'] + "/beta_binom_gt_all_fit_yes/chrom{chrom}.gt.txt", chrom=chroms),
        gs=expand(config['geu_dir'] + "/fsnp_GT/GBR_chr{chrom}.tab", chrom=chroms),
        blackHapCounts=config['phaser_blackHapCounts']
    params:
        samp=samples_geu
    threads:
        10
    output:
        out=config['output_dir'] + "/figures_paper/geu_methods_compare.png"
    script:
        "Scripts/compare_methods_geu.R"        

rule exchangeability_gb:
    """Assess variability in posterior after model training with different samples and concordance on NA12878 comparing model training with different samples. samp4gt is a randomly selected sample from GEUVADIS. Makes Figure 4A and Supp Figure 6A. """
    input:
        fit_gb=config['bottle_dir'] + "/fit_beta_binom/NA12878_stan_beta_binom_fit.rds",
        fit_geu=expand(config['geu_dir'] + "/fit_beta_binom/{samp_gt}_stan_beta_binom_fit.rds", samp_gt=samp4gt),
        fit_pool=config['bottle_dir'] + "/fit_beta_binom/pool_stan_beta_binom_fit.rds",
        gt=expand(config['bottle_dir'] + "/beta_binom_gt_fit_yes/genome_bottle_model_trained_other_samples/gb.{samp}.chrom{chrom}.gt.txt", samp=samples_gb, chrom=chroms),
        gt_geu= expand(config['bottle_dir'] + "/beta_binom_gt_fit_yes/genome_bottle_geu/train{samp_train}_gt{samp}.chrom{chrom}.gt.txt" , samp_train=samp4gt, samp="NA12878", chrom=chroms),
        gt_pool=expand(config['bottle_dir'] + "/beta_binom_gt_fit_yes/genome_bottle_pool/{samp}.chrom{chrom}.gt.txt", samp="NA12878", chrom=chroms),
        blackHapCounts=config['phaser_blackHapCounts'],
        gs=config['bottle_dir'] + "/gold_standard_fSNPs/gs_gt_fsnps.tab",
    output:
        out=config['output_dir'] + "/figures_paper/gb_other_posteriors.png"
    script:
         "Scripts/exchange_gb.R"



rule exchengeability_geu:
    """Assess the accuracy on genotypes after model training with the same sample as for calling genotypes, with genome in a bottle NA12878 or with a pool of GEUVADIS samples. Makes Figure 4B and Supp Figure 6B."""
    input:
        fit_geu=expand(config['geu_dir'] + "/fit_beta_binom/{samp_train}_stan_beta_binom_fit.rds", samp_train= samp4gt),
        fit_pool= config['geu_dir'] + "/fit_beta_binom/pool_stan_beta_binom_fit.rds",
        gt_gb=expand(config['geu_dir'] + "/beta_binom_gt_fit_default/train{samp_train}_gt{samp}.chrom{chrom}.gt.txt" , samp_train="NA12878", samp=samples_geu, chrom=chroms),
        gt=expand(config['geu_dir'] + "/beta_binom_gt_fit_yes/{samp}.chrom{chrom}.gt.txt", samp=samples_geu, chrom=chroms),
        gt_pool=expand(config['geu_dir'] + "//beta_binom_gt_fit_pool/{samp}.chrom{chrom}.gt.txt", samp=samples_geu, chrom=chroms),
        blackHapCounts=config['phaser_blackHapCounts'],
        gs=expand(config['geu_dir'] + "/fsnp_GT/GBR_chr{chrom}.tab", chrom=chroms)        
    params:
        train=samp4train,
        gt=samp4gt
    threads:
        10
    output:
        out=config['output_dir'] + "/figures_paper/geu_other_posteriors.png"
    script:
         "Scripts/exchange_geu.R"
         


rule compare_posterior_gb_geu:
    input:
        fit_gb=expand(config['bottle_dir'] + "/fit_beta_binom/{samp}_stan_beta_binom_fit.rds", samp=sample_bottle),
        fit_geu=expand(config['geu_dir'] + "/fit_beta_binomial/{samp}_stan_beta_binom_fit.rds", samp=samp4gt),
    output:
        out=config['output_dir'] + "/figures_paper/mu_qq_gb_geu_posterior.png"
    script:
        "Scripts/qq_posterior_gb_geu.R"         
 


rule peac_compare_methods:
    """Compare GT calls from RNA-seq using all mehtods (mpileup, beta binomial, gatk and freebayes). Use DNA calls as gold standard. Based on previous QCs I excluded samples with very poor concordance. This will make Figures 2C-3C, Supp Figure 5."""
    input:
        dna=expand(config['peac_dir'] + "/call_var/fSNP/{chrom}.fSNPs.DNA.txt",chrom=chroms),
        fsnps=expand(config['fSNP_dir'] + "/chr.{chrom}.eaf.fSNP.RP.genes.txt", chrom=chroms),
        gt_bb=expand(config['peac_dir'] + "/beta_binom_gt_all_fit_yes/chrom{chrom}.gt.txt", chrom=chroms),
        hc=gatk(expand(config['peac_dir'] + "/gatk/GTQC_joint_filt/chr{chrom}.table", chrom=chroms)),
        freebayes=expand(config['peac_dir'] + "/freebayes/chr.{chrom}.table", chrom=chroms),
        mpileup=expand(config['peac_dir'] + "/mpileup/chr{chrom}_Q20.table", chrom=chroms), 
        blacklistBed=config['phaser_blacklist'],
        blackHapCounts=config['phaser_blackHapCounts'],
        sample_info=config["sample_meta"],
    params:
        chrom=chroms,
        ex_samples=["QMUL2009049", "QMUL2009042", "QMUL2009048", "QMUL2009044", "QMUL2010027", "QMUL2010022", "QMUL2009052", "QMUL2010042", "QMUL2009039", "QMUL2010002"]
    threads:
        10
    output:
        out=config['output_dir'] + "/figures_paper/peac_methods_depth.png"
    script:
        "Scripts/compare_methods_peac.R"


rule exchangeability_peac:
    """Check gt accuracy after training model with gb sample, geu sample or a pool of PEAC samples"""
    input:
        dna=expand(config['peac_dir'] + "/call_var/fSNP/{chrom}.fSNPs.DNA.txt",chrom=chroms),
        fsnps=expand(config['fSNP_dir'] + "/chr.{chrom}.eaf.fSNP.RP.genes.txt", chrom=chroms),
        gt_same=expand(config['peac_dir'] + "/fit_per_sample_uniqe_reads_fit_yes/{ind}.chrom{chrom}.gt.txt", ind=samples_peac, chrom=chroms),
        gt_gb=expand(config['peac_dir'] + "/beta_binom_gt_fit_default/{ind}.chrom{chrom}.gt.txt", ind=samples_peac, chrom=chroms),
        gt_geu=expand(config['peac_dir'] + "/beta_binom_gt_fit_geu/{ind}.chrom{chrom}.gt.txt", ind=samples_peac, chrom=chroms),
        gt_pool=gatk(expand(config['peac_dir'] + "/beta_binom_gt_fit_pool/{ind}.chrom{chrom}.gt.txt", ind=samples_peac, chrom=chroms)),
        blacklistBed=config['phaser_blacklist'],
        blackHapCounts=config['phaser_blackHapCounts'],
        sample_info=config["sample_meta"],
        fit_pool=gatk(config['peac_dir'] + "/fit_beta_binom/pool_stan_beta_binom_fit.rds"),
    params:
        chrom=chroms,
        ex_samples=["QMUL2009049", "QMUL2009042", "QMUL2009048", "QMUL2009044", "QMUL2010027", "QMUL2010022", "QMUL2009052", "QMUL2010042", "QMUL2009039", "QMUL2010002"],
        geu_sample=["HG00243"],
        fit_dir=config['peac_dir'] + "/fit_beta_binom",
    threads:
        10
    output:
        out=config['output_dir'] + "/figures/peac_training_other.png"
    script:
        "Scripts/exchange_peac.R"
        
     
