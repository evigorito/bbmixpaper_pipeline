library(baseqtlpaper)


gb_compare(hc_f = snakemake@input[['hc']],
           mpileup_f = snakemake@input[['mpileup']],
           fb_f = snakemake@input[['fb']],
           bb_fp = snakemake@input[['bb']],
           blackbed = snakemake@input[['blackHapCounts']],
           hla=28477797:33448354,
           threshold=c(0.1, 0.9),
           gs_f = snakemake@input[['gs']],
           samp = snakemake@params[['samp']],
           out = snakemake@output[['out']])

