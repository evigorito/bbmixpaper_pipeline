import os
import re

# Based on same script in RNA_call/Scripts but generalised for reg expression
def bam_rename(bamPath, outNames, outPath, reg='(NA[0-9]+).rmdup.sort.bam'):
    """ Rule to created files with full path to bam and sample name and second file with full path to bam"""
    f=open(outNames, "w+")
    f2=open(outPath, "w+")
    for bam in bamPath:
        base=os.path.basename(bam)
        name=re.split(reg, base)[1]
        f.write(bam + " " + name + "\n")
        f2.write(bam + "\n")
    f.close()
    f2.close()

bam_rename(snakemake.input['bam'],
           str(snakemake.output['bamSampNames']),
           str(snakemake.output['bamPath']),
           snakemake.params['reg'])


    
