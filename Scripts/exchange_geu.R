library(baseqtlpaper)

geu_exchange(fit_geu_f = snakemake@input[['fit_geu']],,
              fit_pool_f = snakemake@input[['fit_pool']],
              gt_bottle = snakemake@input[['gt_gb']],
              gt_f = snakemake@input[['gt']],
              gt_pool_f = snakemake@input[['gt_pool']],
              samp.gt = snakemake@params[['gt']],
              samp.train = snakemake@params[['train']],
              blackbed = snakemake@input[['blackHapCounts']],
              gs_f = snakemake@input[['gs']],
              out = snakemake@output[['out']])

